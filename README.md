# Dans ce document vous trouverez le protocole d’utilisation et les règles du jeu.
- Règles du jeu :  
Les règles sont simples, votre but est de survire le plus longtemps possible. Vous incarnez   la bulle bleue qui tente de ne pas se faire éclater par ses ennemis les bulles rouges. Pour cela vous devez fuir, la bulle s’occupera de tirer sur les bulles ennemies ! 
- Protocole d’utilisation :   
Rappel : Avoir le et toutes les images dans le même dossier !  
  - Exécuter le script BubbleSurvivor.py via votre éditeur de code, une fenêtre pygame devrai alors s’ouvrir.  
  - Pour lancer une partie il suffit de cliquer n’importe où sur la fenêtre Pygame. Bon jeu à vous !  
- Touches :  
Pointeur de la souris : Pour se déplacer la bulle suit votre pointeur de souris.  
Espace : Mets pause au jeu.  
Echap : Vous fait quitter la partie actuelle. 
