#Importation des modules necessaires
from enum import Enum
import time
import  pygame
import math
import random

#Initialisation de Pygame
pygame.init()


#Fonction de l'enumeration pour les etats du jeu
class etat(Enum):
    start = 1
    playing= 2
    pause = 3
    dead = 4
    quit = 5
    error = 6

#Definition de la classe du joueur
class joueur : 
    #Initialisation des attributs du joueur
    #Position, taille, vie, attaque ...
    def __init__(self):
        self.position=(400,360)
        self.taille=40
        self.rmin=5
        self.rmax=100
        self.vmax=10
        self.vie=6
        self.vieInit=6
        self.attaque=1
        self.couleur_rond= (0,255,255)
        self.tirs = []
        self.dateTir=time.time()
        self.dateDegat=time.time()
        self.delayTir=0.3
        self.delayDegat=0.5
        self.tirTouche=0
        self.image = pygame.image.load("joueur.png") # l'image du joueur
        self.image=pygame.transform.scale(self.image, (self.taille,self.taille))

    #Affichage du joueur sur l'ecran
    def afficher(self, jeu):
        if(jeu.etat.name=="playing" or jeu.etat.name=="pause"):
            jeu.screen.blit(self.image, (self.position[0]-self.taille/2, self.position[1]-self.taille/2))

    #Deplacement du joueur en fonction de la position de la souris
    def deplacer(self,jeu, x, y):
        if(jeu.etat.name=="playing"):
            distance=math.sqrt((x-self.position[0])*(x-self.position[0])+(y-self.position[1])*(y-self.position[1]))
            if(distance>self.rmin):
                x1=self.position[0]
                y1=self.position[1]
                dxu=(x-x1)/distance
                dyu=(y-y1)/distance
                vitesse=max((distance-self.rmin)/(self.rmax-self.rmin)*self.vmax,self.vmax)
                x1+=dxu*vitesse
                y1+=dyu*vitesse
                x1=max(0,min(x1,jeu.width))
                y1=max(0,min(y1,jeu.height))
                self.position=(x1,y1)

    #Methode pour que le joueur tire
    def tirer(self,jeu):
        if ((time.time()-self.dateTir)>=self.delayTir) and jeu.etat.name=="playing" :
            self.tirs.append(tir(self,jeu))
            self.dateTir=time.time()

    #Methode pour detecter et gerer les degats infligees au joueur
    def killTirs(self,jeu):
        killTirs=[]
        for i in range(len(self.tirs)):
            if self.tirs[i].killTir(jeu) == False :
                killTirs.append(i)
        killTirs.reverse()
        for i in killTirs:
            self.tirs.pop(i)
    
    def degat(self,jeu):
        if(jeu.etat.name=="playing"):
            for i in jeu.monstres:
                if (time.time()-self.dateDegat)>=self.delayDegat:
                    distance=math.sqrt((i.position[0]-self.position[0])**2+(i.position[1]-self.position[1])**2)
                    if distance<=self.taille:
                        self.vie-=i.attaque
                        self.dateDegat=time.time()



#Fonction pour les monstres
class monstre :
    #Initialisation des attributs du monstre
    def __init__(self,jeu):
        self.type = 1
        self.position = (0,0)
        self.spawn(jeu.width, jeu.height, jeu.joueur1.position[0], jeu.joueur1.position[1])
        self.taille = 24
        self.vitesse=7
        self.vie = 1
        self.attaque = 1
        self.couleur_rond= (255,0,0)
        self.rmin=20
        self.rpop=200
        self.imageOrigine = pygame.image.load("monstre.png") # l'image du joueur
        self.image=pygame.transform.scale(self.imageOrigine, (self.taille,self.taille))
         
        #Methode pour faire apparaitre les monstres
    def spawn(self, width, height, x, y):
        x1=random.randint(0,width)
        y1=random.randint(0,height)
        distance=math.sqrt((x1-x)**2+(y1-y)**2)
        if (distance>=200):
            self.position=(x1,y1)
        else:
            self.spawn(width, height, x, y)
    
    #Methode pour afficher le monstre sur l'ecran
    def afficher(self,jeu,x,y):
        if(jeu.etat.name=="playing" or jeu.etat.name=="pause") and self.vie>0 :
            jeu.screen.blit(self.image, (self.position[0], self.position[1]))  # Affichage du texte
       
    #Methode pour deplacer le monstre vers le joueur
    def deplacer(self,jeu, x, y):
        if(jeu.etat.name=="playing"):
            distance=math.sqrt((x-self.position[0])*(x-self.position[0])+(y-self.position[1])*(y-self.position[1]))
            if(distance>self.rmin):
                x1=self.position[0]
                y1=self.position[1]
                dxu=(x-x1)/distance
                dyu=(y-y1)/distance
                x1+=dxu*self.vitesse
                y1+=dyu*self.vitesse
                x1=max(0,min(x1,jeu.width))
                y1=max(0,min(y1,jeu.height))
                self.position=(x1,y1)
    
    #Methode pour detecter et gerer les degats infliges au monstre
    def degat(self,joueur,jeu):
        if(jeu.etat.name=="playing"):
            self.vie-=joueur.attaque
            if self.vie<=0 :
                jeu.monstres_tues+=1

#Definition de la classe pour les tirs du joueur
class tir:
    #Initialisation des attributs du tir
    #Taille, vitesse, couleur
    def __init__(self,joueur,jeu):
        self.position = (joueur.position[0],joueur.position[1])
        self.taille = 18
        self.vitesse = 9
        self.couleur=(0,0,0)
        self.image = pygame.image.load("tir.png") # l'image du joueur
        self.image=pygame.transform.scale(self.image, (self.taille,self.taille))
        self.du = jeu.monstres
        monstre=0
        distanceMem=1000
        for i in jeu.monstres:
            distance=math.sqrt((i.position[0]-self.position[0])**2+(i.position[1]-self.position[1])**2)
            if distance<=distanceMem:
                distanceMem=distance
                monstre=i
        dxu=(monstre.position[0]-self.position[0])/distanceMem
        dyu=(monstre.position[1]-self.position[1])/distanceMem
        self.du=(dxu,dyu)
        self.isalive=True
        
    #Methode pour deplacer le tir sur l'ecran
    def deplacer(self,jeu):
        if self.isalive==True and jeu.etat.name=="playing" :
            x=self.position[0] + self.du[0] * self.vitesse
            y=self.position[1] + self.du[1] * self.vitesse
            self.position=(x,y)

    #Methode pour afficher le tir sur l'ecran
    def afficher(self,jeu):
         if((jeu.etat.name=="playing" or jeu.etat.name=="pause") and self.isalive == True):
            jeu.screen.blit(self.image, (self.position[0], self.position[1]))  # Affichage du texte

    #Methode pour detecter et gerer les collisions avec les monstres
    def killTir(self, jeu):
         if(jeu.etat.name=="playing"):
            for i in jeu.monstres:
                distance=math.sqrt((i.position[0]-self.position[0])**2+(i.position[1]-self.position[1])**2)
                if distance <= i.taille :
                    i.degat(jeu.joueur1,jeu)
                    jeu.joueur1.attaque+=0.05
                    jeu.joueur1.tirTouche+=1
                    self.isalive=False
                    return False
            if self.position[0]<=0 or self.position[0] >= jeu.width or self.position[1]<=0 or self.position[1] >= jeu.height :
                self.isalive=False
                return False
            return True


#Definition de la classe pour les boutons
class bouton : 
    def __init__(self,fichier,x,y,width,height):
        self.fichier=fichier
        self.x=x
        self.y=y
        self.width=width
        self.height=height
        self.etat=0
        self.image=pygame.image.load(fichier)
        self.image=pygame.transform.scale(self.image, (width,height))
        self.rect=self.image.get_rect(topleft=(x, y))

#Definition de la classe principale du jeu
class jeu :
    # Initialisation des attributs du jeu
    def __init__(self):
        self.etat = etat.start
        self.width = 1000
        self.height =  600
        self.joueur1=joueur()
        self.screen = pygame.display.set_mode((self.width,self.height))
        self.image = pygame.image.load("fondvert.png") # l'image du fond
        self.image = pygame.transform.scale(self.image, (self.width, self.height))
        self.logo = pygame.image.load("logo.png") # l'image du logo du jeu
        self.logo = pygame.transform.scale(self.logo, (self.width/2, self.height/2))
        self.boutonplay=bouton('play.jpg',(self.width-400)/2,380,400,50)
        self.dateStart=time.time()
        self.monstres_tues = 0  # Nombre de monstres tues
        self.monstres_tues_max=0  # Nombre de monstres tues max
        self.font = pygame.font.SysFont(None, 36)
        self.monstres=[]
        self.afficher()
    #Methode pour afficher les elements du jeu sur l'ecran
    def afficher(self):
        self.screen.blit(self.image,(0,0))
        self.joueur1.afficher(self)
        for i in self.joueur1.tirs:
            i.afficher(self)
        for i in self.monstres:
            i.afficher(self,self.joueur1.position[0],self.joueur1.position[1])
        if(self.etat.name == "start"):
            self.screen.blit(self.boutonplay.image, (self.boutonplay.x, self.boutonplay.y))
            self.screen.blit(self.logo,(self.width/4, self.height/9))
            # Affichage du nombre de monstres tues
            pygame.draw.rect(self.screen, (0, 0, 0), pygame.Rect(10, 10, 150, 30))  # Cadre bleu
            monstres_tues_text_max = self.font.render("record : " + str(self.monstres_tues_max), True, (255, 255, 255))  # Conversion du nombre de monstres tues en texte blanc
            self.screen.blit(monstres_tues_text_max, (15, 15))  # Affichage du texte
        if(self.etat.name=="playing" or self.etat.name=="pause"):
            # Affichage des barres de vie
            pygame.draw.rect(self.screen, (255, 0, 0), pygame.Rect(self.width - 150, 10, 120, 30))  # Barre de vie rouge
            pygame.draw.rect(self.screen, (0, 255, 0), pygame.Rect(self.width - 150, 10, self.joueur1.vie * 20, 30))  # Barre de vie verte proportionnelle au nombre de vies
            # Affichage du nombre de monstres tues
            pygame.draw.rect(self.screen, (0, 0, 0), pygame.Rect(10, 10, 100, 30))  # Cadre bleu
            monstres_tues_text = self.font.render(str(self.monstres_tues), True, (255, 255, 255))  # Conversion du nombre de monstres tues en texte blanc
            self.screen.blit(monstres_tues_text, (15, 15))  # Affichage du texte
        pygame.display.flip()
        
    #Methode pour fusionner les monstres proches
    def fusion(self):
        for i in range(len(self.monstres)-1):
            monstres=self.monstres
            j=self.monstres[i]
            for k in monstres:
                distance=math.sqrt((j.position[0]-k.position[0])**2+(j.position[1]-k.position[1])**2)
                if distance<(j.taille+k.taille) and distance > 0:
                    k.taille=(j.taille+k.taille)/1.2
                    k.image=pygame.transform.scale(k.imageOrigine, (k.taille, k.taille))
                    k.vie=j.vie+k.vie
                    k.vitesse=(j.vitesse+k.vitesse)/1.8
                    k.attaque=j.attaque+k.attaque
                    k.position=((j.position[0]+k.position[0])/2,(j.position[1]+k.position[1])/2)
                    self.monstres.pop(i)
                    return
                
    #Methode pour deplacer les elements du jeu
    def deplacer(self, x, y):
        self.joueur1.deplacer(self,x,y)
        for i in self.joueur1.tirs:
            i.deplacer(self)
        for i in self.monstres:
            i.deplacer(self,self.joueur1.position[0],self.joueur1.position[1])
            
    #Methode pour executer le jeu   
    def run(self,x,y):
        self.joueur1.tirer(self)
        self.fusion()
        self.deplacer(x,y)
        self.joueur1.degat(self)
        self.joueur1.killTirs(self)
        if len(self.monstres)<math.log(time.time()-self.dateStart+1)+4:
            self.monstres.append(monstre(self))
        killMonstre=[]
        for i in range(len(self.monstres)):
            if self.monstres[i].vie<=0 :
                killMonstre.append(i)
        killMonstre.reverse()
        for i in killMonstre:
            self.monstres.pop(i)
        if(self.etat.name != "pause"):
            self.afficher()
        if self.joueur1.vie<=0:
            self.restart()

    #Methode pour gerer le clic afin de lancer le jeu
    def clickplay(self):
        self.etat=etat.playing
        
    #Methode pour detecter les clics de la souris
    def click(self,x,y):
        if (self.etat.name=="start" and self.boutonplay.rect.collidepoint((x,y))):
            self.clickplay()
            
    #Methode pour mettre en paue ou reprendre le jeu
    def pause(self):
        if (self.etat.name== "playing"):
            self.etat =  etat.pause
        elif (self.etat.name == "pause"):
            self.etat = etat.playing

     #Methode pour redemarrer le jeu
    def restart(self):
        if (self.etat.name != "start"):
            self.etat =  etat.start
            self.monstres=[]
            self.joueur1=joueur()
            self.dateStart=time.time()
            self.monstres_tues_max=max(self.monstres_tues_max,self.monstres_tues)
            self.monstres_tues=0
        #Methode pour detecter les touches qui sont presse
    def keydown(self,key):
        if key == pygame.K_SPACE:
            self.pause()
        elif key == pygame.K_ESCAPE:
            self.restart()


        
#Creation de l'instance du jeu
jeu1 = jeu()
souris=(0,0)
continuer = True

#Boucle principale du jeu
while continuer :
    # Gestion des evenements Pygame
    for evenement in pygame.event.get():
        if evenement.type == pygame.QUIT:
            continuer = False
        if evenement.type == pygame.MOUSEMOTION:
            souris=evenement.pos
        if evenement.type == pygame.MOUSEBUTTONDOWN:
            if evenement.button == 1 :
                jeu1.click(evenement.pos[0],evenement.pos[1])
        if evenement.type == pygame.KEYDOWN:
            jeu1.keydown(evenement.key)
    #Execution du jeu avec la position de la souris
    jeu1.run(souris[0],souris[1])
    time.sleep(0.04)
    
#Fermeture de la fenetre pygame
pygame.display.quit()